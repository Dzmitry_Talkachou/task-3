package by.dzmitry.handler.runner;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import static by.dzmitry.handler.constant.ModificatorEnum.*;

import by.dzmitry.handler.entity.Composite;
import by.dzmitry.handler.parser.ParserText;
import by.dzmitry.handler.service.HandlerService;
import by.dzmitry.handler.service.SaveText;

public class Runner {
	static {
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
	}

	public static void main(String[] args) {
		
		final String resource = "resource/";
		String text = new ParserText().createString(resource + "input/sample.txt");
		
		HandlerService handlerService = new HandlerService().addOption(ORIGNAL_TEXT);
		
		HandlerService handlerServiceOne = new HandlerService().addOption(SWAP_FIRST_AND_LAST_WORDS_IN_ALL_SENTENCES)
                                                               .addOption(SWAP_FIRST_AND_LAST_LETTER_IN_ALL_WORDS)
                                                         	   .addOption(REPLACE_WORD)
                                                               .addOption(DELETE_WORD);
				
		HandlerService handlerServiceTwo = new HandlerService().addOption(REMOVE_ALL_LETTERS_AS_FIRST_LETTER_WORD)
				                                               .addOption(REMOVE_ALL_LETTERS_AS_LAST_LETTER_WORD)
				                                               .addOption(SWAP_FIRST_AND_LAST_LETTER_IN_ALL_WORDS);
		
		Composite compositeText = handlerService.composeText(text);
		Composite compositeTextOne = handlerServiceOne.composeText(text);
		Composite compositeTextTwo = handlerServiceTwo.composeText(text);

		SaveText.write(resource + "output/sample_created.txt", compositeText.write());
		SaveText.write(resource + "output/sample_to_string.txt", compositeText.toString());
		SaveText.write(resource + "output/sample_created_1.txt", compositeTextOne.write());
		SaveText.write(resource + "output/sample_created_2.txt", compositeTextTwo.write());

	}
}