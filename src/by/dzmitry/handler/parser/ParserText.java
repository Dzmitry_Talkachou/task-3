package by.dzmitry.handler.parser;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.log4j.Logger;

public class ParserText {
	private static final Logger LOGGER = Logger.getLogger(ParserText.class);

	public String createString(String file) {
		String text = null;
		try {
			FileInputStream inFile = new FileInputStream(file);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			text = new String(str);
		} catch (IOException c) {
			LOGGER.fatal("File " + file + " not found.");
			throw new RuntimeException(c);
		}
		return text;
	}
}
