package by.dzmitry.handler.service;

import java.util.ArrayList;

import by.dzmitry.handler.entity.Composite;
import by.dzmitry.handler.constant.ModificatorEnum;
import by.dzmitry.handler.entity.Component;
import by.dzmitry.handler.entity.Letter;

public class ModificatorServce {

	public Composite modifyWord(Composite wordComposite, ArrayList<ModificatorEnum> selector) {
		for (ModificatorEnum modify : selector) {
			switch (modify) {

			case SWAP_FIRST_AND_LAST_LETTER_IN_ALL_WORDS:
				wordComposite = swap(wordComposite);
				break;

			case REMOVE_ALL_LETTERS_AS_FIRST_LETTER_WORD:
				wordComposite = removeAllLettersAsLetterOnPosition(wordComposite, 0);
				break;

			case REMOVE_ALL_LETTERS_AS_LAST_LETTER_WORD:
				wordComposite = removeAllLettersAsLetterOnPosition(wordComposite, -1);
				break;

			case ORIGNAL_TEXT:// Nothing to modify.
				break;
			default:// Nothing to modify.
			}
		}
		return wordComposite;
	}

	public Composite modifySentence(Composite sentenceComposite, ArrayList<ModificatorEnum> selector) {
		for (ModificatorEnum modify : selector) {
			switch (modify) {

			case SWAP_FIRST_AND_LAST_WORDS_IN_ALL_SENTENCES:
				sentenceComposite = swap(sentenceComposite);
				break;

			case REPLACE_WORD:
				sentenceComposite = replaceWords(sentenceComposite, 5, "System.exit(0)");
				break;

			case DELETE_WORD:
				sentenceComposite = replaceWords(sentenceComposite, 4, "");
				break;

			case ORIGNAL_TEXT:// Nothing to modify.
				break;

			default:// Nothing to modify.
			}
		}
		return sentenceComposite;
	}

	public Composite modifyParagraph(Composite paragraphComposite, ArrayList<ModificatorEnum> selector) {
		for (ModificatorEnum modify : selector) {
			switch (modify) {

			case ORIGNAL_TEXT:// Nothing to modify.
				break;

			default:// Nothing to modify.
			}
		}
		return paragraphComposite;
	}

	private Composite replaceWords(Composite sentenceComposite, int wordSize, String replacedWord) {
		Composite replacedWordComposite = stringToComposite(replacedWord);
		int i = 0;
		if (!sentenceComposite.isEmpty()) {
			do {
				if (wordSize == sentenceComposite.get(i).write().length()) {
					sentenceComposite.set(i, replacedWordComposite);
				}
			} while (++i < sentenceComposite.size());
		}
		return sentenceComposite;
	}

	private Composite swap(Composite composite) {
		CheckerRegex checker = new CheckerRegex();
		int firstCompPosition = 0;
		int lastCompPosition = 0;
		if (composite.size() > 1) {
			for (int i = 0; i < composite.size(); i++) {
				if (checker.isLetters(composite.get(i).write()) && firstCompPosition == 0) {
					firstCompPosition = i;
				}
				if (checker.isLetters(composite.get(i).write())) {
					lastCompPosition = i;
				}
			}
			Component firstComposite = composite.get(firstCompPosition);
			composite.set(firstCompPosition, composite.get(lastCompPosition));
			composite.set(lastCompPosition, firstComposite);
		}
		return composite;

	}

	private Composite removeAllLettersAsLetterOnPosition(Composite wordComposite, int letterPosition) {
		Composite wordsCompositeNew = new Composite();
		ArrayList<Component> modifiedWord = new ArrayList<Component>();
		char[] wordCharArray = null;
		char removedSymbol;
		if (!wordComposite.write().isEmpty()) {
			wordCharArray = wordComposite.write().toCharArray();
			if (letterPosition > wordCharArray.length || letterPosition < 0) {
				letterPosition = wordCharArray.length - 1;
			}
			removedSymbol = wordCharArray[letterPosition];
			for (char chr : wordCharArray) {
				if (removedSymbol != chr) {
					modifiedWord.add(new Letter(chr));
				}
			}
			wordsCompositeNew.add(new Composite(modifiedWord));
			modifiedWord.clear();
		}
		return wordsCompositeNew;
	}

	private Composite stringToComposite(String word) {
		Composite wordCompostie = new Composite();
		for (char chr : word.toCharArray()) {
			wordCompostie.add(new Letter(chr));
		}
		return wordCompostie;
	}
}