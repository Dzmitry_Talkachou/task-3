
package by.dzmitry.handler.service;

import static by.dzmitry.handler.constant.RegexConstant.*;

public class CheckerRegex {

	public boolean isLetters(String string) {
		return string.replaceAll(NOT_LETTER, "").equals(string);
	}

	public boolean isLetter(char chr) {
		return String.valueOf(chr).replaceAll(NOT_LETTER, "").equals(String.valueOf(chr));
	}

	public boolean isEndSentence(String string) {
		return !string.replaceAll(SENTENCE_END, "").equals(string);
	}

	public boolean isEndParagaph(String string) {
		return !string.replaceAll(PARAGRAPH_END, "").equals(string);
	}

	public boolean isCode(String string) {
		return !string.replaceAll(CODE, "").equals(string);
	}

}