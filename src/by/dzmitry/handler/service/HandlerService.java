package by.dzmitry.handler.service;

import java.util.ArrayList;
import java.util.Arrays;

import static by.dzmitry.handler.constant.RegexConstant.*;

import by.dzmitry.handler.constant.ModificatorEnum;
import by.dzmitry.handler.entity.Component;
import by.dzmitry.handler.entity.Composite;
import by.dzmitry.handler.entity.Letter;
import by.dzmitry.handler.entity.Listing;
import by.dzmitry.handler.entity.NotLetter;
import by.dzmitry.handler.service.ModificatorServce;

public class HandlerService {

	private ArrayList<ModificatorEnum> modifiyOptions = new ArrayList<ModificatorEnum>();

	public HandlerService addOption(ModificatorEnum option) {
		modifiyOptions.add(option);
		return this;
	}

	public Composite composeText(String text) {

		Composite wordsComposite = parseWords(pasreListings(text));
		Composite sentensesComposite = parseSenteces(wordsComposite);
		Composite paragraphComposite = parseParagraph(sentensesComposite);

		return parseText(paragraphComposite);
	}

	private ArrayList<String> pasreListings(String text) {

		ArrayList<String> textCollection = new ArrayList<String>();
		String[] textSplit = text.split(CODE_START);
		
		if (textSplit.length > 0) {
			textCollection.addAll(Arrays.asList(textSplit[0].split(" ")));
		}
		String[] textSplitAgain;
		for (String txt : textSplit) {
			textSplitAgain = txt.split(CODE_END);
			if (textSplitAgain.length > 1) {
				textCollection.add(CODE_START + textSplitAgain[0] + CODE_END);
				textCollection.addAll(Arrays.asList(textSplitAgain[1].split(" ")));
			}
		}
		return textCollection;
	}

	private Composite parseWords(ArrayList<String> text) {
		ModificatorServce modifyService = new ModificatorServce();
		Composite wordsComposite = new Composite();
		CheckerRegex checker = new CheckerRegex();

		for (String word : text) {
			ArrayList<Component> letters = new ArrayList<Component>();
			if (!checker.isCode(word)) {
				for (char chr : word.toCharArray()) {
					if (checker.isLetter(chr)) {
						letters.add(new Letter(chr));
					} else {
						if (!letters.isEmpty()) {
							wordsComposite.add(modifyService.modifyWord(new Composite(letters), modifiyOptions));
							letters.clear();
						}
						wordsComposite.add(new NotLetter(chr));
					}
				}
				if (!letters.isEmpty()) {
					wordsComposite.add(modifyService.modifyWord(new Composite(letters), modifiyOptions));
				}
				wordsComposite.add(new NotLetter(' '));
			} else {
				if (!wordsComposite.isEmpty()) {
					wordsComposite.remove(wordsComposite.size() - 1);
				}
				wordsComposite.add(new Listing(word));
			}
		}
		return wordsComposite;
	}

	private Composite parseSenteces(Composite wordsComposite) {

		ModificatorServce modifyService = new ModificatorServce();
		Composite sentencesComposite = new Composite();
		ArrayList<Component> sentence = new ArrayList<Component>();
		CheckerRegex checker = new CheckerRegex();
		String sentencseCheck = "";

		for (Component word : wordsComposite.getComponents()) {
			sentence.add(word);
			sentencseCheck += word.write();
			if (checker.isEndSentence(sentencseCheck)) {
				sentencesComposite.add(modifyService.modifySentence(new Composite(sentence), modifiyOptions));
				sentence.clear();
				sentencseCheck = "";
			}
		}

		return sentencesComposite;
	}

	private Composite parseParagraph(Composite sentencesComposite) {

		ModificatorServce modifyService = new ModificatorServce();
		Composite paragraphComposite = new Composite();
		ArrayList<Component> paragraph = new ArrayList<Component>();
		CheckerRegex checker = new CheckerRegex();
		String paragraphCheck;

		for (Component sentence : sentencesComposite.getComponents()) {
			paragraph.add(sentence);
			paragraphCheck = sentence.write();
			if (checker.isEndParagaph(paragraphCheck)) {
				paragraphComposite.add(modifyService.modifyParagraph(new Composite(paragraph), modifiyOptions));
				paragraph.clear();
			}
		}
		return paragraphComposite;
	}

	private Composite parseText(Composite paragraphComposite) {
		Composite textComposite = new Composite();
		textComposite.add(paragraphComposite);
		return textComposite;
	}
}