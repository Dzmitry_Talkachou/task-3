package by.dzmitry.handler.service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

public class SaveText {
	private static final Logger LOGGER = Logger.getLogger(SaveText.class);

	public static void write(String fileName, String text) {
		File file = new File(fileName);
		try {
			file.createNewFile();
			PrintWriter out = new PrintWriter(file.getAbsoluteFile());
			try {
				out.print(text);
			} finally {
				out.close();
			}
		} catch (IOException e) {
			LOGGER.fatal("RuntimeException " + e);
			throw new RuntimeException(e);
		}
	}
}