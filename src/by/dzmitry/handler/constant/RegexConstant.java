package by.dzmitry.handler.constant;

public class RegexConstant {

	public static final String NOT_LETTER = "[^\\w�]";
	public static final String SENTENCE_END = "[.|?|!|;|:][\\s|\\S|\\n|\\S\\n]";
	public static final String CODE = "<%(.|\n|\r)+?%>";
	public static final String CODE_START = "<%";
	public static final String CODE_END = "%>";
	public static final String PARAGRAPH_END = "[\\n\\r]";

}
