package by.dzmitry.handler.entity;

public class NotLetter implements Component {
	
	private char notLetter;

	public NotLetter(char notLetter) {
		this.notLetter = notLetter;
	}

	@Override
	public String write() {
		return String.valueOf(notLetter);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + notLetter;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotLetter other = (NotLetter) obj;
		if (notLetter != other.notLetter)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + notLetter + "]";
	}
}