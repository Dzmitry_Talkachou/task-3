package by.dzmitry.handler.entity;

public class Listing implements Component {

	private String listing;

	@Override
	public String write() {
		return listing;
	}

	public Listing(String listing) {

		this.listing = listing;
	}

	@Override
	public String toString() {
		return "List[" + listing + "]\n";
	}
}