package by.dzmitry.handler.entity;

import java.util.ArrayList;

public class Composite implements Component {

	private ArrayList<Component> components = new ArrayList<>();

	public Composite() {
	}

	public Composite(ArrayList<Component> components) {
		this.components.addAll(components);
	}

	public Composite(Composite composite) {
		this.components.add(composite);
	}

	public ArrayList<Component> getComponents() {
		return components;
	}

	public void add(Component component) {
		components.add(component);
	}

	public void clear() {
		components.clear();
	}

	public int size() {
		return components.size();
	}

	public Component remove(int key) {
		return components.remove(key);
	}

	public void set(int key, Component component) {
		this.components.set(key, component);
	}

	public Component get(int key) {
		return this.components.get(key);
	}

	public boolean remove(Component component) {
		return components.remove(component);
	}

	public boolean isEmpty() {
		return components.isEmpty();
	}

	public boolean addAll(ArrayList<Component> components) {
		return this.components.addAll(components);
	}

	@Override
	public String write() {
		String string = "";
		for (Component component : this.components) {
			string += component.write();
		}
		return string;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((components == null) ? 0 : components.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Composite other = (Composite) obj;
		if (components == null) {
			if (other.components != null)
				return false;
		} else if (!components.equals(other.components))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return  components + "\n";
	}
}