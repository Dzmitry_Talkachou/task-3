package by.dzmitry.handler.entity;

public interface Component {
	
	public String write();

}
