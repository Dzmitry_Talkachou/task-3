package by.dzmitry.handler.entity;

public class Letter implements Component {

	private char letter;

	public Letter(char letter) {
		this.letter = letter;
	}
	
	@Override
	public String write() {
		return String.valueOf(letter);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + letter;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Letter other = (Letter) obj;
		if (letter != other.letter)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.valueOf(letter);
	}
}